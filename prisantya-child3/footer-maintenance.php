   <!-- Footer -->
   <footer class="py-5 bg-dark">
      <div class="container">
        <p> <div class="bottomMenu">
              <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>  
    </div> 
        </p>
        <p class="m-0 text-center text-white">Copyright &copy; <?php echo get_option('blogname').'  '.date('Y'); ?></p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <?php wp_footer(); ?>
  </body>
  
</html>