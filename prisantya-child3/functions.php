<?php


require_once get_template_directory(). '/options-framework/options-framework.php';


add_filter( 'template_include', 'maintenance_page_template', 99 );

function maintenance_page_template( $template ) {

	if ( of_get_option( 'maintenance' ) ) {
		$new_template = locate_template( array( 'maintenance.php' ) );
		if ( !empty( $new_template ) ) {
			return $new_template;
		}
	}

	return $template;
}