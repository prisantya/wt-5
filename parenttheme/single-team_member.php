<?php
/**
 * The template for displaying custom post team member
 *
 * @package WordPress
 * @subpackage prisantya
 * @since Twenty Nineteen 1.0
 */

get_header(); ?>

   <!-- Page Content -->
<div class="container">

<div class="row">

  <!-- Post Content Column -->
  <div class="col-lg-8">

    <!-- Title -->
    <h1 class="mt-4"><?php the_title( '<h1 align="center" style="font-size:30px;">', '</h1>' ); ?></h1>

    <!-- Author -->
    <p class="lead">
      Position
      <a href="#"><?php   echo esc_html( get_post_meta( get_the_ID(), 'cp_position', true ) ); ?></a>
    </p>

    <hr>

    <hr>

    <!-- Preview Image -->
	<?php
		$team_image=get_post_meta( get_the_ID(), 'cp_image', true );
		if ($team_image!='')
		    echo "<img class='img-fluid rounded' src='".wp_upload_dir()['baseurl']."/wt2/".$team_image."' height='400' width='400'>   <br />"; 
	?>
    
    <hr>

    <!-- Post Content -->
	<table style="border:none">
		<tr>
			<td width="25%"> <strong>Email </strong> </td> <td>: <?php echo esc_html( get_post_meta( get_the_ID(), 'cp_email', true ) ); ?> </td> 
		</tr>
		<tr>
			<td width="25%"> <strong>Phone: </strong> </td> <td>: <?php echo esc_html( get_post_meta( get_the_ID(), 'cp_phone', true ) ); ?> </td>
		</tr>
		<tr>
			<td width="25%"> <strong>Website: </strong> </td> <td>: <?php echo esc_html( get_post_meta( get_the_ID(), 'cp_website', true ) ); ?> </td>
		</tr>
		</table>
    <hr>

  </div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->


<?php get_footer(); ?>
