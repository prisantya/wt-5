<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org
 *
 * @package WordPress
 * @subpackage prisantya
 * @since Twenty Nineteen 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html lang="<?php echo get_locale(); ?>">

  <head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo get_option( 'blogdescription' );?>">
    <meta name="author" content="">  
	<?php wp_head();?>
  </head>

  <body>
  <?php 
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
    
  ?>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo site_url();?>">
          <?php echo '<img src="' . esc_url( $custom_logo_url ) . '" alt="" width="25" height="25"> ';
                echo '  '.get_option( 'blogname' ); ?>
        </a>
        
        <div  >
          <ul >
            
            <?php wp_nav_menu( array( 'theme_location' => 'top' ) ); ?>
          </ul>
        </div>
      </div>
    </nav>
