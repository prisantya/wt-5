<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage prisantya
 * @since Twenty Nineteen 1.0
 * @version 1.0
 */


get_header(); ?>

 <!-- Page Content -->
<div class="container">

<div class="row">

		<?php 
		
			if ( have_posts() ) : while ( have_posts() ) : the_post();
  	
				get_template_part( 'content', get_post_format() );
  
			endwhile; 
		
			//next_posts_link( 'Older posts' );
			//previous_posts_link( 'Newer posts' ); 
			endif; 
		?>

  <?php get_sidebar(); ?>
  
  </div>
      <!-- /.row -->
</div>
    <!-- /.container -->

<?php get_footer(); ?>
