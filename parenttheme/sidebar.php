<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage prisantya
 * @since Twenty Nineteen 1.0
 */
?>

<!-- Sidebar Widgets Column -->
<div class="col-md-4">

<!-- Search Widget 
<div class="card my-4">
  <h5 class="card-header">Search</h5>
  <div class="card-body">
    <div class="input-group">
      <?php /* the_widget('WP_Widget_Search'); */?>
    </div>
  </div>
</div>

 Categories Widget 
<div class="card my-4">
  <h5 class="card-header">Categories</h5>
  <div class="card-body">
    <div class="row">
        <?php /* the_widget('WP_Widget_Categories'); */?>
    </div>
  </div>
</div>

Side Widget 
<div class="card my-4">
  <h5 class="card-header">Side Widget</h5>
  <div class="card-body">
   
  </div>
</div>
-->

<!-- Dynamic Widget -->
<div class="card my-4">
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
    <div class="card-body">
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div>
<?php endif; ?>
</div>

</div>
