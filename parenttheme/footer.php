<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage prisantya
 * @since Twenty Nineteen 1.0
 * @version 1.0
 */

?>   
   <!-- Footer -->
   <footer class="py-5 bg-dark">
      <div class="container">
        <p> <div class="bottomMenu">
              <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>  
    </div> 
        </p>
        <p class="m-0 text-center text-white">Copyright &copy; <?php echo get_option('blogname').'  '.date('Y'); ?></p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <?php wp_footer(); ?>
  </body>
  
</html>