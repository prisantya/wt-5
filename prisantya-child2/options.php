<?php

function optionsframework_option_name() {
	// mytheme slug
	return 'prisantya-child2';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';
	$options = array();

	$options[] = array(
		'name' => __( 'Basic Settings', 'theme-textdomain' ),
		'type' => 'heading'
	);
//    Upload Logo, Maintenance Mode
	
    $options[] = array(
        'name' => "Upload Logo",
        'desc' => 'Upload Logo',
        'id' => 'logos',
        'type' => 'upload'
    );

    $options[]=array(
        'name'=> 'Show Sidebar',
        'desc'=> 'Show sidebar',
        'id' => 'sidebar',
        'type'=> 'checkbox'
    );
    
    $options[]=array(
        'name'=>'Limit Post',
        'desc'=>'Limit Post in HomePage',
        'id'=>'post',
        'type'=>'text'
    );

    return $options;
    
}

