<?php get_header(); ?>

 <!-- Page Content -->
<div class="container">

<!-- Jumbotron Header -->
<header class="jumbotron my-4">
        <h1 class="display-3">A Warm Welcome!</h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
        <a href="#" class="btn btn-primary btn-lg">Call to action!</a>
      </header>

      <!-- Page Features -->
      <div class="row text-center">
      <?php 
        // the query
        $limitpost= of_get_option('post');
       
        $the_query = new WP_Query( array(
            'posts_per_page' => $limitpost,
        )); 
         if ( $the_query->have_posts() ) : 
         while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="col-lg-3 col-md-6 mb-4">
            <div class="card">
                <?php 
                   echo (has_post_thumbnail()? the_post_thumbnail(array(325,500), ['class' => 'card-img-top']):'<img class="card-img-top" src="http://placehold.it/500x325" alt="">');
                   
                ?>              
                <div class="card-body">
                    <h4 class="card-title"><?php the_title(); ?> </h4>
                    <p class="card-text"><?php the_excerpt();  ?> </p>
                </div>
                <!-- end of class card body-->
                <div class="card-footer">
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary">Find Out More!</a>
                </div>    
                <!-- end of class card footer-->
            </div>
        <!-- end of class card-->
        </div>
         <?php endwhile;  wp_reset_postdata();  endif;?>
        <!-- end of square post-->
      </div>
      <!-- end of post features-->
    <?php
      if (of_get_option('sidebar')) { get_sidebar(); }
    ?>
</div>
    <!-- /.container -->

<?php get_footer(); ?>