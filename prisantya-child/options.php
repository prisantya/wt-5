<?php

function optionsframework_option_name() {
	// mytheme slug
	return 'prisantya-child';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';
	$options = array();

	$options[] = array(
		'name' => __( 'Basic Settings', 'theme-textdomain' ),
		'type' => 'heading'
	);
//    Upload Logo, Footer Copyright, Blog Description
	$options[] = array(
	    'name' => "Blog Description",
        'type' => 'textarea',
        'id' => 'blogdescription'
    );

    $options[] = array(
        'name' => "Footer Copyright",
        'type' => 'text',
        'id' => 'copyright'
    );
    $options[] = array(
        'name' => "Upload Logo",
        'desc' => 'Upload Logo',
        'id' => 'logo',
        'type' => 'upload'
    );
    

    return $options;
    
}

